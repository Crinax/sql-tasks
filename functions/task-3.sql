SELECT Customers.name FROM Orders
JOIN Customers ON Customers.row_id = Orders.customer_id
JOIN OrderItems ON OrderItems.order_id = Orders.row_id
WHERE Orders.registered_at
  BETWEEN DATE('2020-01-01')
  AND DATE('2020-01-01') + INTERVAL '1 year'
  AND OrderItems.name = 'Кассовый аппарат'
GROUP BY Customers.name;
