DROP FUNCTION IF EXISTS calculate_total_price_for_orders_group;
CREATE OR REPLACE FUNCTION calculate_total_price_for_orders_group(id INT)
RETURNS TABLE(row_id INT,  group_name VARCHAR(255), customer_id INT)
AS $$
  WITH RECURSIVE result AS (
    SELECT row_id, group_name, customer_id
    FROM Orders
    WHERE row_id = id

    UNION

    SELECT Orders.row_id, Orders.group_name, Orders.customer_id
    FROM Orders
      JOIN result
      ON Orders.parent_id = result.row_id
  )
  SELECT  FROM result;
$$ LANGUAGE SQL;


-- SELECT * FROM calculate_total_price_for_orders_group(1) AS total_price;
