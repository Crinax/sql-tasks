DROP FUNCTION IF EXISTS select_orders_by_item_name;
CREATE OR REPLACE FUNCTION select_orders_by_item_name(item_name VARCHAR(255))
RETURNS TABLE(order_id INT, customer_name VARCHAR(255), items_count INT)
AS $$
  SELECT
    order_id,
    Customers.name "customer_name",
    COUNT(*) AS items_count
  FROM OrderItems
  JOIN Orders ON Orders.row_id = order_id
  JOIN Customers ON Customers.row_id = Orders.customer_id
  WHERE OrderItems.name = item_name
  GROUP BY order_id, customer_name
$$ LANGUAGE SQL;


-- SELECT * FROM select_orders_by_item_name('Факс');
